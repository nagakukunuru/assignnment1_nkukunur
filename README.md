Naga Kukunuru
nkukunur@mtu.edu

“If you want to walk fast, walk alone. But if you want to walk far, walk together.”
-Ratan Tata.
